#!/usr/bin/env python

"""
test_pacman-mirrors
----------------------------------

Tests for `pacman-mirrors` module.
"""

import unittest
from unittest.mock import patch

from pacman_mirrors.functions import httpFn, config_setup, cliFn, defaultFn
from pacman_mirrors.pacman_mirrors import PacmanMirrors
from . import mock_configuration as mock


test_conf = {
    "branch": "stable",
    "branches": mock.BRANCHES,
    "config_file": mock.CONFIG_FILE,
    "custom_file": mock.CUSTOM_FILE,
    "method": "rank",
    "mirror_file": mock.MIRROR_FILE,
    "mirror_list": mock.MIRROR_LIST,
    "no_update": False,
    "country_pool": [],
    "protocols": [],
    "repo_arch": mock.REPO_ARCH,
    "status_file": mock.STATUS_FILE,
    "ssl_verify": True,
    "test_file": mock.TEST_FILE,
    "url_mirrors_json": mock.URL_MIRROR_JSON,
    "url_status_json": mock.URL_STATUS_JSON,
    "work_dir": mock.WORK_DIR,
    "arm": False,
    "timeout": 2,
}


class TestHttpFn(unittest.TestCase):
    """Pacman Mirrors Test suite"""

    def setUp(self):
        pass

    @patch("os.getuid")
    @patch.object(httpFn, "get_ip_country")
    @patch.object(config_setup, "setup_config")
    def test_geoip_available(
        self, mock_build_config, mock_get_geoip_country, mock_os_getuid
    ):
        """TEST: geoip country IS avaiable"""
        mock_os_getuid.return_value = 0
        mock_build_config.return_value = test_conf
        mock_get_geoip_country.return_value = ["Denmark"]
        args = "--geoip"
        cmd = mock.TEST_CMD + args
        with unittest.mock.patch("sys.argv", cmd.split()):
            app = PacmanMirrors()
            app.config = config_setup.setup_config(app)
            cliFn.parse_command_line(app, True)
            defaultFn.load_default_mirror_pool(app)
            app.selected_countries = httpFn.get_ip_country()
            assert app.selected_countries == ["Denmark"]

    def tearDown(self):
        pass


if __name__ == "__main__":
    unittest.main()
